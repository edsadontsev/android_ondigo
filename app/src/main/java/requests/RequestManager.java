package requests;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import requests.response_models.CheckPhoneResponse;
import requests.response_models.Error;
import requests.response_models.LogCallResponse;
import requests.response_models.Search;
import requests.response_models.User;
import retrofit2.Call;

import static constants.Constants.Requests.REQUEST_BODY_MEDIA_TYPE;
import static utils.AppUtils.isNull;

public class RequestManager {
    private IRequestService mRequestService;

    private RequestManager() {
        init();
    }

    private static class RequestManagerHolder {
        public static final RequestManager INSTANCE = new RequestManager();
    }

    public static RequestManager get() {
        return RequestManagerHolder.INSTANCE;
    }

    private void init() {
        mRequestService = ServiceFactory.createRetrofitService(IRequestService.class);
    }

    public RequestBody createRequestBody(Object src, JsonObject json) {
        String content = new Gson().toJsonTree(src).toString();
        content = isNull(json) ? content : json.toString();

        return RequestBody.create(MediaType.parse(REQUEST_BODY_MEDIA_TYPE), content);
    }

    public Call<User> getUser(String credentials) {
        return mRequestService.getUser(credentials);
    }

    public Call<Search> search(String credentials, String query) {
        return mRequestService.search(credentials,query);
    }

    public Call<LogCallResponse> logCall(String credentials, RequestBody requestBody) {
        return mRequestService.logCall(credentials,requestBody);
    }

    public Call<CheckPhoneResponse> checkPhone(String credentials, String number) {
        return mRequestService.checkPhone(credentials,number);
    }

}