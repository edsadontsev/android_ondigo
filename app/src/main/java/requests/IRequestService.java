package requests;

import okhttp3.RequestBody;
import requests.response_models.CheckPhoneResponse;
import requests.response_models.Error;
import requests.response_models.LogCallResponse;
import requests.response_models.Search;
import requests.response_models.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


interface IRequestService {

    @GET("me")
    Call<User> getUser(@Query("credentials") String credentials);

    @GET("search")
    Call<Search> search(@Query("credentials") String credentials,
                        @Query("q") String query);

    @POST("call")
    Call<LogCallResponse> logCall(@Query("credentials") String credentials,
                                  @Body RequestBody requestBody);

    @GET("identify")
    Call<CheckPhoneResponse> checkPhone(@Query("credentials") String credentials,
                                        @Query("number") String number);
}