package requests.request_models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CheckPhoneRequest implements Parcelable {

    @SerializedName("number")
    public String number;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.number);
    }

    public CheckPhoneRequest() {
    }

    protected CheckPhoneRequest(Parcel in) {
        this.number = in.readString();
    }

    public static final Parcelable.Creator<CheckPhoneRequest> CREATOR = new Parcelable.Creator<CheckPhoneRequest>() {
        @Override
        public CheckPhoneRequest createFromParcel(Parcel source) {
            return new CheckPhoneRequest(source);
        }

        @Override
        public CheckPhoneRequest[] newArray(int size) {
            return new CheckPhoneRequest[size];
        }
    };
}
