package requests.request_models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class LogCall implements Parcelable {

    @SerializedName("id")
    public String id;

    @SerializedName("contact_id")
    public String contactId;

    @SerializedName("number")
    public String number;

    @SerializedName("country_iso_code")
    public String countryIsoCode;

    @SerializedName("outgoing")
    public boolean outgoing;

    @SerializedName("start")
    public long start;

    @SerializedName("end")
    public long end;

    @SerializedName("sentiment")
    public String sentiment;

    @SerializedName("note")
    public String note;

    @SerializedName("V")
    public String version;

    @SerializedName("missed")
    public boolean missed;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.contactId);
        dest.writeString(this.number);
        dest.writeString(this.countryIsoCode);
        dest.writeByte(this.outgoing ? (byte) 1 : (byte) 0);
        dest.writeLong(this.start);
        dest.writeLong(this.end);
        dest.writeString(this.sentiment);
        dest.writeString(this.note);
        dest.writeString(this.version);
        dest.writeByte(this.missed ? (byte) 1 : (byte) 0);
    }

    public LogCall() {
    }

    protected LogCall(Parcel in) {
        this.id = in.readString();
        this.contactId = in.readString();
        this.number = in.readString();
        this.countryIsoCode = in.readString();
        this.outgoing = in.readByte() != 0;
        this.start = in.readLong();
        this.end = in.readLong();
        this.sentiment = in.readString();
        this.note = in.readString();
        this.version = in.readString();
        this.missed = in.readByte() != 0;
    }

    public static final Parcelable.Creator<LogCall> CREATOR = new Parcelable.Creator<LogCall>() {
        @Override
        public LogCall createFromParcel(Parcel source) {
            return new LogCall(source);
        }

        @Override
        public LogCall[] newArray(int size) {
            return new LogCall[size];
        }
    };
}
