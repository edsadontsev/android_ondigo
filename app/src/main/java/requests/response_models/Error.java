package requests.response_models;

import com.google.gson.annotations.SerializedName;

public class Error {

    @SerializedName("code")
    public int code;

    @SerializedName("message")
    public String message;
}

