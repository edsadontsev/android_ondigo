package requests.response_models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CheckPhoneResponse implements Parcelable {

    @SerializedName("id")
    public String id;

    @SerializedName("type")
    public String type;

    @SerializedName("name")
    public String name;

    @SerializedName("title")
    public String title;

    @SerializedName("company")
    public String company;

    @SerializedName("url")
    public String url;

    @SerializedName("description")
    public String description;

    @SerializedName("code")
    public int code;

    @SerializedName("message")
    public String message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.type);
        dest.writeString(this.name);
        dest.writeString(this.title);
        dest.writeString(this.company);
        dest.writeString(this.url);
        dest.writeString(this.description);
        dest.writeInt(this.code);
        dest.writeString(this.message);
    }

    public CheckPhoneResponse() {
    }

    protected CheckPhoneResponse(Parcel in) {
        this.id = in.readString();
        this.type = in.readString();
        this.name = in.readString();
        this.title = in.readString();
        this.company = in.readString();
        this.url = in.readString();
        this.description = in.readString();
        this.code = in.readInt();
        this.message = in.readString();
    }

    public static final Parcelable.Creator<CheckPhoneResponse> CREATOR = new Parcelable.Creator<CheckPhoneResponse>() {
        @Override
        public CheckPhoneResponse createFromParcel(Parcel source) {
            return new CheckPhoneResponse(source);
        }

        @Override
        public CheckPhoneResponse[] newArray(int size) {
            return new CheckPhoneResponse[size];
        }
    };
}