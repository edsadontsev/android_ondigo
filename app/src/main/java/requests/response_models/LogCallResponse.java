package requests.response_models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class LogCallResponse implements Parcelable {

    @SerializedName("id")
    public String id;

    @SerializedName("contact_id")
    public String contactId;

    @SerializedName("code")
    public int code;

    @SerializedName("message")
    public String message;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.contactId);
        dest.writeInt(this.code);
        dest.writeString(this.message);
    }

    public LogCallResponse() {
    }

    protected LogCallResponse(Parcel in) {
        this.id = in.readString();
        this.contactId = in.readString();
        this.code = in.readInt();
        this.message = in.readString();
    }

    public static final Parcelable.Creator<LogCallResponse> CREATOR = new Parcelable.Creator<LogCallResponse>() {
        @Override
        public LogCallResponse createFromParcel(Parcel source) {
            return new LogCallResponse(source);
        }

        @Override
        public LogCallResponse[] newArray(int size) {
            return new LogCallResponse[size];
        }
    };
}
