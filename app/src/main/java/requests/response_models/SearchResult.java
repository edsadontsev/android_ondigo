package requests.response_models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchResult implements Parcelable {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("type")
    public String type;

    @SerializedName("title")
    public String title;

    @SerializedName("company")
    public String company;

    @SerializedName("url")
    public String url;

    @SerializedName("email")
    public String email;

    @SerializedName("phones")
    public ArrayList<Phone> phones;

    @SerializedName("code")
    public int code;

    @SerializedName("message")
    public String message;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.type);
        dest.writeString(this.title);
        dest.writeString(this.company);
        dest.writeString(this.url);
        dest.writeString(this.email);
        dest.writeTypedList(this.phones);
        dest.writeInt(this.code);
        dest.writeString(this.message);
    }

    public SearchResult() {
    }

    protected SearchResult(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.type = in.readString();
        this.title = in.readString();
        this.company = in.readString();
        this.url = in.readString();
        this.email = in.readString();
        this.phones = in.createTypedArrayList(Phone.CREATOR);
        this.code = in.readInt();
        this.message = in.readString();
    }

    public static final Parcelable.Creator<SearchResult> CREATOR = new Parcelable.Creator<SearchResult>() {
        @Override
        public SearchResult createFromParcel(Parcel source) {
            return new SearchResult(source);
        }

        @Override
        public SearchResult[] newArray(int size) {
            return new SearchResult[size];
        }
    };
}
