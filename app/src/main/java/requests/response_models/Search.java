package requests.response_models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Search implements Parcelable {

    @SerializedName("q")
    public String q;

    @SerializedName("results")
    public ArrayList<SearchResult> results;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.q);
        dest.writeTypedList(this.results);
    }

    public Search() {
    }

    protected Search(Parcel in) {
        this.q = in.readString();
        this.results = in.createTypedArrayList(SearchResult.CREATOR);
    }

    public static final Parcelable.Creator<Search> CREATOR = new Parcelable.Creator<Search>() {
        @Override
        public Search createFromParcel(Parcel source) {
            return new Search(source);
        }

        @Override
        public Search[] newArray(int size) {
            return new Search[size];
        }
    };
}
