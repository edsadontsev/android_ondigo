package requests.response_models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CallArchive implements Parcelable {

    @SerializedName("id")
    public String id;

    @SerializedName("contact_id")
    public String contactId;

    @SerializedName("number")
    public String number;

    @SerializedName("country_iso_code")
    public String countryIsoCode;

    @SerializedName("outgoing")
    public boolean outgoing;

    @SerializedName("start")
    public long start;

    @SerializedName("end")
    public long end;

    @SerializedName("sentiment")
    public String sentiment;

    @SerializedName("note")
    public String note;

    @SerializedName("name")
    public String name;

    @SerializedName("company")
    public String company;

    @SerializedName("call_type")
    public String callType;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.contactId);
        dest.writeString(this.number);
        dest.writeString(this.countryIsoCode);
        dest.writeByte(this.outgoing ? (byte) 1 : (byte) 0);
        dest.writeLong(this.start);
        dest.writeLong(this.end);
        dest.writeString(this.sentiment);
        dest.writeString(this.note);
        dest.writeString(this.name);
        dest.writeString(this.company);
        dest.writeString(this.callType);
    }

    public CallArchive() {
    }

    protected CallArchive(Parcel in) {
        this.id = in.readString();
        this.contactId = in.readString();
        this.number = in.readString();
        this.countryIsoCode = in.readString();
        this.outgoing = in.readByte() != 0;
        this.start = in.readLong();
        this.end = in.readLong();
        this.sentiment = in.readString();
        this.note = in.readString();
        this.name = in.readString();
        this.company = in.readString();
        this.callType = in.readString();
    }

    public static final Parcelable.Creator<CallArchive> CREATOR = new Parcelable.Creator<CallArchive>() {
        @Override
        public CallArchive createFromParcel(Parcel source) {
            return new CallArchive(source);
        }

        @Override
        public CallArchive[] newArray(int size) {
            return new CallArchive[size];
        }
    };
}
