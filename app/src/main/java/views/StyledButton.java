package views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.ondigo.R;

public class StyledButton extends android.support.v7.widget.AppCompatButton {

    public StyledButton(Context context) {
        super(context);
    }

    public StyledButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public StyledButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.StyledTextView);
        String customFontAssetsPath = typedArray.getString(R.styleable.StyledTextView_font);

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), customFontAssetsPath);
        setTypeface(typeface);

        typedArray.recycle();
    }
}

