package fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import interfaces.IViewsBinder;

import static utils.AppUtils.isNull;

public abstract class BaseDialogFragment extends DialogFragment implements IViewsBinder {
    protected Unbinder mUnbinder;
    protected Activity mActivity;
    protected View mContentView;
    protected OnDialogDismissed mOnDialogDismissedListener;

    public abstract int getLayoutResId();
    public abstract void initFragment();

    public interface OnDialogDismissed {
        void onDismissed();
    }

    public BaseDialogFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (isNull(mContentView)) {
            mContentView = inflater.inflate(getLayoutResId(), container, false);
        }

        bindViews();
        initFragment();

        return mContentView;
    }

    @Override
    public void onDestroy() {
        unbindViews();
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void bindViews() {
        mUnbinder = ButterKnife.bind(this, mContentView);
    }

    @Override
    public void unbindViews() {
        if (!isNull(mUnbinder)) mUnbinder.unbind();
    }

    public void setOnDismissedListener (OnDialogDismissed listener) {
        mOnDialogDismissedListener = listener;
    }
}
