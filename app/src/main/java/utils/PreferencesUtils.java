package utils;

import java.util.ArrayList;

import io.paperdb.Book;
import io.paperdb.Paper;
import requests.response_models.CallArchive;

import static constants.Constants.CREDENTIALS;
import static constants.Constants.Extras.CALL;
import static constants.Constants.NO_DATA;

public class PreferencesUtils {

    public static void saveCallList(ArrayList<CallArchive> list) {
        Paper.book().write(CALL, list);
    }

    public static ArrayList<CallArchive> readCallList() {
        Book paperBook = Paper.book();

        return paperBook.read(CALL);
    }

}