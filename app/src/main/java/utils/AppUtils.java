package utils;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.DimenRes;
import android.support.v7.app.AppCompatDelegate;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ondigo.MainApplication;
import com.ondigo.R;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import requests.response_models.SearchResult;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static constants.Constants.NO_DATA;

public class AppUtils {

    public static boolean isNull(Object... objects) {
        for (Object obj : objects) {
            if (obj == null) return true;
        }

        return false;
    }

    public static String getVersion(Context context){
        String versionName;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionName = "android-"+packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            versionName = NO_DATA;
        }
        return versionName;
    }

    public static String convertDate(long dateTime){

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dateTime);


        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DAY_OF_YEAR, -1);
        Calendar c2 = Calendar.getInstance();
        c2.setTimeInMillis(dateTime);

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd hh:mm aa", Locale.ENGLISH);
        String formattedDate = dateFormat.format(dateTime);
        Date tempTime = calendar.getTime();
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa", Locale.ENGLISH);
        String formattedTime = timeFormat.format(tempTime);


        if(DateUtils.isToday(dateTime))
            return "Today, "+formattedTime;
        else if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
                && c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR))
            return "Yesterday, "+formattedTime;
        else
            return formattedDate;
    }

    public static int dimensByRes(Context context, @DimenRes int dimensRes) {
        return (int) context.getResources().getDimension(dimensRes);
    }

    public static void showErrorDialog(Context context,String message){
        final Dialog dialog = new Dialog(context, R.style.Dialog);
        dialog.setContentView(R.layout.popup_saved);
        Window window = dialog.getWindow();
        window.setLayout(MATCH_PARENT, WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        RelativeLayout container = (RelativeLayout) dialog.findViewById(R.id.container);
        TextView text = (TextView) dialog.findViewById(R.id.title);
        text.setText(message);

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static boolean isInternetConnection(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info == null) {
                return false;
            }
        }
        return true;
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
}
