package constants;

public interface Constants {

    String NO_DATA = "";
    String CREDENTIALS = "credentials";

    String SALESFORCE_LOGIN_URL = "https://ondigo.me/call-logger/login";
    String SALESFORCE_LOGIN_SUCCESS_URL = "https://ondigo.me/call-logger/connect-email";

    String LOGOUT_POPUP_WINDOW = "logout_popup_window";
    String LOGGED_CALL_POPUP_WINDOW = "logged_call_popup_window";
    String SAVED_POPUP_WINDOW = "logged_call_popup_window";

    interface Requests {
        String ENDPOINT_URL = "https://ondigo.me/call-logger/v1/";
        String REQUEST_BODY_MEDIA_TYPE = "application/json";
    }

    interface Extras{
        String CONTACTS = "contacts";
        String CALL_FLAG = "call_flag";
        String CALL_ID = "call_id";
        String CALL = "call";
        String TRIAL = "trial";
        String PHONE = "phone";
        String OUTBOUND = "outbound";
        String INBOUND = "inbound";
        String START = "start";
        String END = "end";
        String CALL_ARCHIVE = "call_archive";
        String NOTIFICATION = "notification";
        String NAME = "name";
        String COMPANY = "company";
        String TITLE = "title";
    }

    String POSITIVE = "positive";
    String NEUTRAL = "neutral";
    String NEGATIVE = "negative";
}
