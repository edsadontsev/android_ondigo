package dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;

import com.ondigo.R;

import java.util.ArrayList;

import activities.AddSummaryActivity;
import butterknife.OnClick;
import fragments.BaseDialogFragment;
import requests.request_models.LogCall;
import requests.response_models.CallArchive;
import requests.response_models.SearchResult;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static constants.Constants.Extras.CALL;
import static constants.Constants.Extras.CALL_ID;
import static constants.Constants.Extras.CONTACTS;
import static utils.AppUtils.isNull;
import static utils.PreferencesUtils.readCallList;
import static utils.PreferencesUtils.saveCallList;

public class LoggedCallDialogFragment extends BaseDialogFragment {

    private SearchResult searchResult;
    private String callId;
    private LogCall logCall;

    @Override
    public int getLayoutResId() {
        return R.layout.popup_logged_call;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        Bundle bundle = getArguments();
        if (!isNull(bundle)) {
            searchResult = bundle.getParcelable(CONTACTS);
            callId = bundle.getString(CALL_ID);
            logCall = bundle.getParcelable(CALL);
            saveCall();
        }

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        initDialogStyle();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if (!isNull(mOnDialogDismissedListener)) mOnDialogDismissedListener.onDismissed();
    }

    @Override
    public void initFragment() {
    }

    @OnClick(R.id.add)
    void addSummary() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(CONTACTS, searchResult);
        bundle.putParcelable(CALL, logCall);
        bundle.putString(CALL_ID, callId);
        Intent intent = new Intent(getActivity(), AddSummaryActivity.class);
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        startActivity(intent);
        dismiss();
    }

    @OnClick(R.id.close)
    void close() {
        dismiss();
    }

    private void initDialogStyle () {
        Dialog dialog = getDialog();
        if (!isNull(dialog)) {
            Window window = dialog.getWindow();
            window.setLayout(MATCH_PARENT, MATCH_PARENT);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void saveCall(){
        ArrayList<CallArchive> list;

        if(!isNull(readCallList()))list = readCallList();
        else list = new ArrayList<>();

        CallArchive call = new CallArchive();
        call.id = logCall.id;
        call.contactId = logCall.contactId;
        call.callType = logCall.outgoing ? getString(R.string.outgoing_call) : getString(R.string.incoming_call);
        call.number = logCall.number;
        call.countryIsoCode = logCall.countryIsoCode;
        call.outgoing = logCall.outgoing;
        call.start = logCall.start;
        call.end = logCall.end;
        call.sentiment = logCall.sentiment;
        call.note = logCall.note;
        call.name = searchResult.name;
        call.company = searchResult.company;

        list.add(call);
        saveCallList(list);
    }
}
