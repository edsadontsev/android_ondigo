package dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.ondigo.R;

import java.util.ArrayList;

import activities.LoginActivity;
import butterknife.OnClick;
import fragments.BaseDialogFragment;
import requests.response_models.CallArchive;
import shared_pref.Save;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static constants.Constants.NO_DATA;
import static utils.AppUtils.isNull;
import static utils.PreferencesUtils.saveCallList;

public class LogOutDialogFragment extends BaseDialogFragment {

    private Save save;

    @Override
    public int getLayoutResId() {
        return R.layout.popup_logout;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        initDialogStyle();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if (!isNull(mOnDialogDismissedListener)) mOnDialogDismissedListener.onDismissed();
    }

    @Override
    public void initFragment() {
    }

    @OnClick(R.id.logout)
    void logout() {
        save = new Save(getActivity());
        save.saveSession(NO_DATA);
        saveCallList(new ArrayList<CallArchive>());
        CookieSyncManager.createInstance(getActivity());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.cancel)
    void cancel() {
        dismiss();
    }

    private void initDialogStyle () {
        Dialog dialog = getDialog();
        if (!isNull(dialog)) {
            Window window = dialog.getWindow();
            window.setLayout(MATCH_PARENT, MATCH_PARENT);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

}
