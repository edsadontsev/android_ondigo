package dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;
import android.widget.TextView;

import com.ondigo.R;

import activities.HomePageActivity;
import butterknife.BindView;
import butterknife.OnClick;
import fragments.BaseDialogFragment;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static constants.Constants.Extras.CALL;
import static constants.Constants.Extras.CALL_ID;
import static constants.Constants.Extras.CONTACTS;
import static constants.Constants.Extras.TRIAL;
import static utils.AppUtils.isNull;

public class SavedDialogFragment extends BaseDialogFragment {

    private boolean flagDialog;

    @BindView(R.id.title) TextView title;

    @Override
    public int getLayoutResId() {
        return R.layout.popup_saved;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        Bundle bundle = getArguments();
        if (!isNull(bundle)) {
            flagDialog = bundle.getBoolean(TRIAL);
        }

        if(flagDialog)  title.setText(getString(R.string.trial_expired));

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        initDialogStyle();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        if (!isNull(mOnDialogDismissedListener)) mOnDialogDismissedListener.onDismissed();

        Intent intent = new Intent(getActivity(), HomePageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void initFragment() {
    }

    @OnClick(R.id.container)
    void close() {
        getActivity().finish();
        dismiss();
    }

    private void initDialogStyle () {
        Dialog dialog = getDialog();
        if (!isNull(dialog)) {
            Window window = dialog.getWindow();
            if (window != null) {
                window.setLayout(MATCH_PARENT, MATCH_PARENT);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        }
    }

}
