package adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ondigo.R;

import java.util.ArrayList;
import java.util.List;

import activities.AddSummaryActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import requests.response_models.SearchResult;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static constants.Constants.Extras.CONTACTS;
import static constants.Constants.Extras.PHONE;
import static constants.Constants.NO_DATA;

public class LogCallListAdapter extends BaseAdapter {

    private Context ctx;
    private LayoutInflater lInflater;
    private ArrayList<SearchResult> objects;

    @BindView(R.id.name) TextView name;
    @BindView(R.id.company) TextView company;
    @BindView(R.id.item) RelativeLayout item;
    @BindView(R.id.image) ImageView image;

    public LogCallListAdapter(Context context, ArrayList<SearchResult> results) {
        ctx = context;
        objects = results;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    public void add(List<SearchResult> data) {
        this.objects.addAll(data);
    }
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {


        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.list_item, null, true);
        }
        ButterKnife.bind(this,view);

        SearchResult result = getResult(position);
        name.setText(result.name);
        company.setText(result.company);
        image.setImageResource(R.drawable.logcall);

        item.setOnClickListener(new LogCallListAdapter.OnItemClickListener(result));

        return view;
    }

    private SearchResult getResult(int position) {

        return ((SearchResult) getItem(position));
    }

    private class OnItemClickListener implements View.OnClickListener {
        private SearchResult result;

        OnItemClickListener(SearchResult result) {
            this.result = result;
        }

        @Override
        public void onClick(View v) {

            Bundle bundle = new Bundle();
            bundle.putParcelable(CONTACTS, result);
            bundle.putString(PHONE, result.phones.size()==0 ? NO_DATA : result.phones.get(0).value);
            Intent intent = new Intent(ctx, AddSummaryActivity.class);
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
            intent.putExtras(bundle);
            ctx.startActivity(intent);
        }
    }

}
