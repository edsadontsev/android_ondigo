package adapters;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ondigo.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import requests.response_models.Phone;

import static android.view.View.GONE;

public class ContactListAdapter extends BaseAdapter {

    private Context ctx;
    private LayoutInflater lInflater;
    private ArrayList<Phone> objects;
    private String url;
    private boolean flagEmail;

    @BindView(R.id.name) TextView name;
    @BindView(R.id.company) TextView company;
    @BindView(R.id.image) ImageView image;
    @BindView(R.id.item) RelativeLayout item;

    public ContactListAdapter(Context context, ArrayList<Phone> results, String url, boolean flagEmail) {
        ctx = context;
        objects = results;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.url = url;
        this.flagEmail = flagEmail;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    public void add(List<Phone> data) {
        this.objects.addAll(data);
    }
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.list_item, null, true);
        }
        ButterKnife.bind(this,view);

        Phone result = getResult(position);
        name.setText(result.label);
        company.setText(result.value);


        if(flagEmail) {
            if (position < objects.size() - 2)
                image.setImageResource(R.drawable.makecall);
            else if (position == objects.size() - 2)
                image.setImageResource(R.drawable.email);
            else if (position == objects.size() - 1)
                image.setImageResource(R.drawable.contact_salesforce);
        }
        else{
            if (position < objects.size() - 1)
                image.setImageResource(R.drawable.makecall);
            else if (position == objects.size() - 1)
                image.setImageResource(R.drawable.contact_salesforce);
        }


        item.setOnClickListener(new OnItemClickListener(position,result.value));

        return view;
    }

    private Phone getResult(int position) {

        return ((Phone) getItem(position));
    }

    private class OnItemClickListener implements View.OnClickListener {
        private int position;
        private String phone;

        OnItemClickListener(int position,String phone) {
            this.position = position;
            this.phone = phone;
        }

        @Override
        public void onClick(View v) {
            if(flagEmail) {
                if (position < objects.size() - 2) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                    if (ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
                        ctx.startActivity(intent);
                } else if (position == objects.size() - 2) {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    Uri data = Uri.parse("mailto:" + getResult(position).value);
                    intent.setData(data);
                    ctx.startActivity(Intent.createChooser(intent, null));
                } else if (position == objects.size() - 1) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    ctx.startActivity(browserIntent);
                }
            }
            else{
                if (position < objects.size() - 1) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                    if (ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
                        ctx.startActivity(intent);
                } else if (position == objects.size() - 1) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    ctx.startActivity(browserIntent);
                }
            }

        }
    }

}
