package adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ondigo.R;

import java.util.ArrayList;
import java.util.List;

import activities.AddSummaryActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import requests.response_models.CallArchive;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static constants.Constants.Extras.CALL_ARCHIVE;
import static utils.AppUtils.convertDate;

public class LogCallArchiveAdapter extends BaseAdapter {

    private Context ctx;
    private LayoutInflater lInflater;
    private ArrayList<CallArchive> objects;

    @BindView(R.id.name) TextView name;
    @BindView(R.id.company) TextView company;
    @BindView(R.id.call_type) TextView callType;
    @BindView(R.id.date) TextView date;
    @BindView(R.id.item) RelativeLayout item;

    public LogCallArchiveAdapter(Context context, ArrayList<CallArchive> results) {
        ctx = context;
        objects = results;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    public void add(List<CallArchive> data) {
        this.objects.addAll(data);
    }
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {


        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.list_call_archive_item, null, true);
        }
        ButterKnife.bind(this,view);

        CallArchive result = getResult(position);
        name.setText(result.name);
        company.setText(result.company);
        date.setText(convertDate(result.end));
        callType.setText(result.callType);

        item.setOnClickListener(new LogCallArchiveAdapter.OnItemClickListener(result));

        return view;
    }

    private CallArchive getResult(int position) {

        return ((CallArchive) getItem(position));
    }

    private class OnItemClickListener implements View.OnClickListener {
        private CallArchive result;

        OnItemClickListener(CallArchive result) {
            this.result = result;
        }

        @Override
        public void onClick(View v) {

            Bundle bundle = new Bundle();
            bundle.putParcelable(CALL_ARCHIVE, result);
            Intent intent = new Intent(ctx, AddSummaryActivity.class);
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
            intent.putExtras(bundle);
            ctx.startActivity(intent);
        }
    }
}
