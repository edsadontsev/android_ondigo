package services;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.gson.Gson;
import com.ondigo.R;

import java.util.Date;
import java.util.List;

import activities.AddSummaryActivity;
import activities.HomePageActivity;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import requests.RequestManager;
import requests.request_models.LogCall;
import requests.response_models.CheckPhoneResponse;
import requests.response_models.LogCallResponse;
import requests.response_models.SearchResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shared_pref.Load;
import utils.AppUtils;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static constants.Constants.Extras.CALL;
import static constants.Constants.Extras.CALL_ID;
import static constants.Constants.Extras.COMPANY;
import static constants.Constants.Extras.CONTACTS;
import static constants.Constants.Extras.END;
import static constants.Constants.Extras.INBOUND;
import static constants.Constants.Extras.NAME;
import static constants.Constants.Extras.NOTIFICATION;
import static constants.Constants.Extras.OUTBOUND;
import static constants.Constants.Extras.PHONE;
import static constants.Constants.Extras.START;
import static constants.Constants.Extras.TITLE;
import static constants.Constants.Extras.TRIAL;
import static constants.Constants.NEUTRAL;
import static constants.Constants.NO_DATA;
import static utils.AppUtils.getVersion;
import static utils.AppUtils.isNull;
import static utils.AppUtils.showErrorDialog;

public class CallReceiver extends BroadcastReceiver {

    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static long callStartTime;
    private static boolean isIncoming;
    private static boolean appState;
    private static String savedNumber;

    private Load load;

    @Override
    public void onReceive(Context context, Intent intent) {

        load = new Load(context);

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            appState = topActivity.getPackageName().equals(context.getPackageName());
        }

        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            if (!isNull(intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER)))
                savedNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

        } else {

            String stateStr = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            int state = 0;

            if (TelephonyManager.EXTRA_STATE_IDLE.equals(stateStr)) {
                state = TelephonyManager.CALL_STATE_IDLE;
            } else if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(stateStr)) {
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            } else if (TelephonyManager.EXTRA_STATE_RINGING.equals(stateStr)) {
                state = TelephonyManager.CALL_STATE_RINGING;
            }

            if (!isNull(intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER)))
                savedNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

            onCallStateChanged(context, state, intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER),intent);
        }
    }

    public void onCallStateChanged(final Context context, int state, String number, final Intent intent) {

        if (lastState == state) {
            return;
        }

        switch (state) {
            case TelephonyManager.CALL_STATE_RINGING:
                isIncoming = true;
                callStartTime = System.currentTimeMillis();
                Log.wtf("state","ringing");
                checkPhone(context, load.loadSession(), context.getString(R.string.incoming_call), savedNumber);

                break;

            case TelephonyManager.CALL_STATE_OFFHOOK:
                if (lastState != TelephonyManager.CALL_STATE_RINGING) {
                    isIncoming = false;
                    callStartTime = System.currentTimeMillis();
                    checkPhone(context, load.loadSession(), context.getString(R.string.outgoing_call), savedNumber);

                }
                break;

            case TelephonyManager.CALL_STATE_IDLE:
                if (lastState == TelephonyManager.CALL_STATE_RINGING) {
                    if (appState) sendBroadCast(context, INBOUND);
                    else if (!load.loadSession().equals(NO_DATA)) {

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                String[] PERMISSIONS = {Manifest.permission.CLEAR_APP_CACHE,
                                        Manifest.permission.CALL_PHONE,
                                        Manifest.permission.ACCESS_NETWORK_STATE,
                                        Manifest.permission.READ_CALL_LOG};

                                Log.wtf("permission",hasPermissions(context, PERMISSIONS)+"");
                                if(hasPermissions(context, PERMISSIONS)) {
                                    Uri allCalls = Uri.parse("content://call_log/calls");
                                    Cursor managedCursor = context.getContentResolver().query(allCalls, null, null, null, null);
                                    int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
                                    String callType = NO_DATA;
                                    while (managedCursor.moveToNext()) {
                                        callType = managedCursor.getString(type);
                                    }

                                    int dircode = Integer.parseInt(callType);
                                    switch (dircode) {
                                        case CallLog.Calls.INCOMING_TYPE:
                                            createLogCallRequestBody(context, false, context.getString(R.string.incoming_call_logged), false, false);
                                            break;

                                        case CallLog.Calls.MISSED_TYPE:
                                            createLogCallRequestBody(context, false, context.getString(R.string.missing_call_logged), false, true);
                                            break;
                                    }
                                }
                            }
                        }, 1000);
                    }

                } else if (isIncoming) {
                    if (appState) sendBroadCast(context, INBOUND);
                    else if (!load.loadSession().equals(NO_DATA)) {
                            createLogCallRequestBody(context, false, context.getString(R.string.incoming_call_logged), true, false);
                    }
                } else {
                    if (appState) sendBroadCast(context, OUTBOUND);
                    else if (!load.loadSession().equals(NO_DATA)) {
                        createLogCallRequestBody(context, true, context.getString(R.string.outgoing_call_logged), true, false);
                    }
                }
                break;
        }

        lastState = state;
    }

    public boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void logCall(String credentials, final RequestBody requestBody, final Context context, final LogCall logCall, final String callType, final boolean flagOpenApp) {

        RequestManager.get().logCall(credentials, requestBody).enqueue(new Callback<LogCallResponse>() {

            @Override
            public void onResponse(Call<LogCallResponse> call, final Response<LogCallResponse> response) {
                if (response.isSuccessful()) {
                    Log.wtf("call","logged");
                    if (!isNull(response.body()) && !isNull(response.body().id)) {
                        checkPhone(load.loadSession(), savedNumber, context, logCall, response.body().id, callType,flagOpenApp);
                    }
                }
            }

            @Override
            public void onFailure(Call<LogCallResponse> call, Throwable t) {
            }
        });
    }

    private void checkPhone(final Context context, String credentials, final String message, String number) {
        RequestManager.get().checkPhone(credentials, number).enqueue(new Callback<CheckPhoneResponse>() {

            @Override
            public void onResponse(Call<CheckPhoneResponse> call, final Response<CheckPhoneResponse> response) {
                if (response.isSuccessful()) {
                    Log.wtf("call",call.request().url().toString());
                    if (!isNull(response.body()) && !isNull(response.body().id)) {
                        Log.wtf("user_id",response.body().id);
                        showNotification(context, message, response.body(), false, null, null ,null);
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckPhoneResponse> call, Throwable t) {

            }
        });
    }

    private void checkPhone(String credentials, String number, final Context context, final LogCall logCall, final String callId, final String callType, final boolean flagOpenApp) {
        RequestManager.get().checkPhone(credentials, number).enqueue(new Callback<CheckPhoneResponse>() {

            @Override
            public void onResponse(Call<CheckPhoneResponse> call, final Response<CheckPhoneResponse> response) {
                if (response.isSuccessful()) {
                    if (!isNull(response.body())) {

                        Log.wtf("search name",response.body().name);
                        SearchResult searchResult = new SearchResult();
                        searchResult.name = response.body().name;
                        searchResult.company = response.body().company;
                        searchResult.title = response.body().title;

                        showNotification(context, callType, response.body(), true,searchResult, logCall, callId);

                        if(flagOpenApp) {
                            Intent intentActivity = new Intent(context, HomePageActivity.class);
                            intentActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intentActivity.putExtra(CALL, logCall);
                            intentActivity.putExtra(CONTACTS, searchResult);
                            intentActivity.putExtra(CALL_ID, callId);
                            context.startActivity(intentActivity);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckPhoneResponse> call, Throwable t) {
            }
        });
    }

    private void showNotification(Context context, String message, CheckPhoneResponse checkPhoneResponse, boolean callFlag,SearchResult searchResult,LogCall logCall, String callId) {

        Intent intent = new Intent(context, AddSummaryActivity.class);
        if(callFlag) {
            Log.wtf("call flag",callFlag+"");
            Bundle bundle = new Bundle();
            bundle.putParcelable(CALL, logCall);
            bundle.putString(CALL_ID, callId);
            bundle.putBoolean(NOTIFICATION, true);
            bundle.putParcelable(CONTACTS,searchResult);

            intent.putExtras(bundle);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        String title;

        if (!isNull(checkPhoneResponse.name) && !isNull(checkPhoneResponse.title) && !isNull(checkPhoneResponse.company)) {
            title = checkPhoneResponse.name + ", " + checkPhoneResponse.title + " at " + checkPhoneResponse.company;
        } else if (!isNull(checkPhoneResponse.name) && !isNull(checkPhoneResponse.company)) {
            title = checkPhoneResponse.name + " from " + checkPhoneResponse.company;
        } else
            title = checkPhoneResponse.name;

        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.mipmap.notification_icon_big);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.notification_icon_small)
                //.setLargeIcon(bm)
                .setContentTitle(message)
                .setContentText(title)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);

        if (callFlag) {
            notificationBuilder.setContentIntent(pendingIntent);
            //notificationBuilder.addAction(1,"AAAAA",pendingIntent);
        }

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        String details = checkPhoneResponse.description;
        if (details != null && details.length() > 0) {
            NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
            style.bigText(title + "\n" + details);
            notificationBuilder.setStyle(style);
        } else {
            NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
            style.bigText(title);
            notificationBuilder.setStyle(style);
        }


        Notification notification = notificationBuilder.build();
        notificationManager.notify(0, notification);
    }

    private void createLogCallRequestBody(Context context, boolean type, String callMessage, boolean flagOpenApp, boolean flagMissedCall) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(context.getApplicationContext().TELEPHONY_SERVICE);
        String countryCode = tm.getNetworkCountryIso();
        LogCall logCall = new LogCall();
        logCall.id = NO_DATA;
        logCall.contactId = NO_DATA;
        logCall.countryIsoCode = countryCode;
        logCall.outgoing = type;
        logCall.missed = flagMissedCall;
        logCall.start = callStartTime;
        logCall.end = System.currentTimeMillis();
        logCall.sentiment = NEUTRAL;
        logCall.note = NO_DATA;
        logCall.number = savedNumber;
        logCall.version = getVersion(context);

        String content = new Gson().toJsonTree(logCall).toString();
        Log.wtf("json",content);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), content);
        logCall(load.loadSession(), requestBody, context, logCall, callMessage, flagOpenApp);
    }

    private void sendBroadCast(Context context, String callType) {
        Intent intent = new Intent(callType);
        intent.putExtra(PHONE, savedNumber);
        intent.putExtra(START, callStartTime);
        intent.putExtra(END, System.currentTimeMillis());
        context.sendBroadcast(intent);
    }

}