package interfaces;

public interface IViewsBinder {
    void bindViews();
    void unbindViews();
}
