package com.ondigo;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import io.paperdb.Paper;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        initPaper();
    }

    private void initPaper() {
        Paper.init(getApplicationContext());
    }
}
