package activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.ondigo.R;

import butterknife.BindView;
import shared_pref.Save;

import static constants.Constants.CREDENTIALS;
import static constants.Constants.SALESFORCE_LOGIN_SUCCESS_URL;
import static constants.Constants.SALESFORCE_LOGIN_URL;
import static utils.AppUtils.isNull;

public class SalesforceActivity extends BaseActivity{

    private boolean flag;
    private Save save = new Save(this);

    @BindView(R.id.web_view) WebView webView;
    @BindView(R.id.progress) ProgressBar progressBar;

    @Override
    public int getLayoutResId() {
        return R.layout.salesforce_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initWebView();
    }

    private void initWebView(){
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webView, url);

                if(!flag) {
                    progressBar.setVisibility(View.GONE);
                    webView.setVisibility(View.VISIBLE);
                }

                String cookies = CookieManager.getInstance().getCookie(url);
                if(!isNull(cookies)) {
                    if(cookies.contains(CREDENTIALS)) getCredential(cookies);
                }

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if(url.equals(SALESFORCE_LOGIN_SUCCESS_URL)) {
                    webView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    flag = true;
                }

                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        webView.loadUrl(SALESFORCE_LOGIN_URL);
    }

    private void getCredential(String cookies){
        String[] temp=cookies.split(";");
        String credentials = "";

        for (String ar1 : temp ){
            if(ar1.contains(CREDENTIALS)){
                String[] temp1=ar1.split("=");
                credentials = temp1[1];
                save.saveSession(credentials);

                Intent intent = new Intent(this, HomePageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                break;
            }
        }
    }
}
