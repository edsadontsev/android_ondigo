package activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ondigo.R;

import java.util.ArrayList;
import java.util.Collections;

import adapters.LogCallArchiveAdapter;
import adapters.LogCallListAdapter;
import adapters.MakeCallArchiveAdapter;
import adapters.MakeCallListAdapter;
import butterknife.BindView;
import fragments.BaseDialogFragment;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import requests.RequestManager;
import requests.request_models.LogCall;
import requests.response_models.CallArchive;
import requests.response_models.LogCallResponse;
import requests.response_models.Search;
import requests.response_models.SearchResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shared_pref.Load;

import static adapters.MakeCallArchiveAdapter.callArchive;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.view.View.GONE;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static constants.Constants.Extras.CALL;
import static constants.Constants.Extras.CALL_FLAG;
import static constants.Constants.Extras.CALL_ID;
import static constants.Constants.Extras.CONTACTS;
import static constants.Constants.Extras.END;
import static constants.Constants.Extras.OUTBOUND;
import static constants.Constants.Extras.PHONE;
import static constants.Constants.Extras.START;
import static constants.Constants.NEUTRAL;
import static constants.Constants.NO_DATA;
import static utils.AppUtils.getVersion;
import static utils.AppUtils.isInternetConnection;
import static utils.AppUtils.isNull;
import static utils.AppUtils.showErrorDialog;
import static utils.PreferencesUtils.readCallList;
import static utils.PreferencesUtils.saveCallList;

public class SearchActivity extends BaseActivity implements BaseDialogFragment.OnDialogDismissed {

    private MakeCallListAdapter makeCallListAdapter;
    private LogCallListAdapter logCallListAdapter;
    private LogCallArchiveAdapter logCallArchiveAdapter;
    private MakeCallArchiveAdapter makeCallArchiveAdapter;
    private Context context = this;;
    private boolean callFlag;
    private Call<Search> searchRequest;
    private boolean listFlag;
    private LogCall logCall;
    private SearchResult searchResult;
    private Call<LogCallResponse> request;
    private Load load = new Load(this);

    private int countSymbols;

    @BindView(R.id.search) EditText search;
    @BindView(R.id.list) ListView list;
    @BindView(R.id.search_hint_layout) RelativeLayout searchHintLayout;
    @BindView(R.id.progress) ProgressBar progressBar;

    @Override
    public int getLayoutResId() {
        return R.layout.search_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fetchExtras();
        initListeners();
    }

    @Override
    protected void onResume(){
        super.onResume();
        listFlag = true;

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(OUTBOUND);

        registerReceiver(callDataReceiver, intentFilter);

        search.setText(NO_DATA);
        searchHintLayout.setVisibility(View.VISIBLE);
        list.setVisibility(GONE);
        //initCallArchiveList();

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        try  {
            unregisterReceiver(callDataReceiver);
        }
        catch (IllegalArgumentException e) {

        }

        if(!isNull(request))
            request.cancel();

        if(!isNull(searchRequest))
            searchRequest.cancel();
    }

    private void fetchExtras() {
        Intent intent = getIntent();
        if (!isNull(intent)) {
            callFlag = intent.getBooleanExtra(CALL_FLAG,false);
            listFlag = true;

        }
    }

    private void initCallArchiveList(){
        ArrayList<CallArchive> allCalls = readCallList();

        if(isNull(allCalls) || allCalls.size()==0){
            list.setVisibility(GONE);
            searchHintLayout.setVisibility(View.VISIBLE);
        }
        else if(!callFlag){
            list.setVisibility(GONE);
            searchHintLayout.setVisibility(View.VISIBLE);
        }
        else{
            Collections.reverse(allCalls);
            makeCallArchiveAdapter = new MakeCallArchiveAdapter(context,allCalls);
            list.setAdapter(makeCallArchiveAdapter);
        }
    }

    private void initListeners(){
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                countSymbols = search.getText().toString().length();

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                list.setVisibility(GONE);
                if(!isNull(searchRequest)) {
                    progressBar.setVisibility(GONE);
                    searchRequest.cancel();
                }

                if(search.getText().toString().length()>2){
                    if((search.getText().toString().length()<countSymbols && isInternetConnection(context)) || search.getText().toString().length()>countSymbols ) {

                        if (listFlag)
                            unregisterReceiver(callDataReceiver);

                        listFlag = false;

                        progressBar.setVisibility(View.VISIBLE);
                        searchHintLayout.setVisibility(GONE);
                        search(load.loadSession(), search.getText().toString());
                    }
                }
                else{
                    list.setVisibility(GONE);
                    searchHintLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void search(String credentials, String query){
        if(isInternetConnection(this)) {
            searchRequest = RequestManager.get().search(credentials, query);
            searchRequest.enqueue(new Callback<Search>() {

                @Override
                public void onResponse(Call<Search> call, final Response<Search> response) {
                    Log.wtf("call", call.request().url().toString());
                    if (response.isSuccessful()) {

                        if(response.body().results.isEmpty())
                            list.setVisibility(GONE);

                        progressBar.setVisibility(GONE);
                        if (callFlag) {
                            makeCallListAdapter = new MakeCallListAdapter(context, response.body().results);
                            list.setAdapter(makeCallListAdapter);
                        } else {
                            logCallListAdapter = new LogCallListAdapter(context, response.body().results);
                            list.setAdapter(logCallListAdapter);
                        }

                        list.setItemsCanFocus(true);
                        if (response.body().results.size() != 0) {
                            list.setVisibility(View.VISIBLE);
                            searchHintLayout.setVisibility(GONE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Search> call, Throwable t) {
                }
            });
        }
        else{
            progressBar.setVisibility(GONE);
            showErrorDialog(this,getString(R.string.check_internet_connection));
        }

    }

    private final BroadcastReceiver callDataReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(OUTBOUND)) {
                String number = intent.getStringExtra(PHONE);
                long start = intent.getLongExtra(START, 0);
                long end = intent.getLongExtra(END, 0);

                TelephonyManager tm = (TelephonyManager)getSystemService(getApplicationContext().TELEPHONY_SERVICE);
                String countryCode = tm.getNetworkCountryIso();
                logCall = new LogCall();
                logCall.id = callArchive.id;
                logCall.contactId = callArchive.contactId;
                logCall.countryIsoCode = countryCode;
                logCall.outgoing = true;
                logCall.missed = false;
                logCall.start = start;
                logCall.end = end;
                logCall.sentiment = NEUTRAL;
                logCall.note = NO_DATA;
                logCall.number = number;
                logCall.version = getVersion(context);

                searchResult = new SearchResult();
                searchResult.name = callArchive.name;
                searchResult.company = callArchive.company;

                String content = new Gson().toJsonTree(logCall).toString();
                Log.wtf("json",content);
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), content);
                logCall(load.loadSession(),requestBody);
            }
        }

    };

    private void logCall(String credentials, final RequestBody requestBody){
        if(isInternetConnection(this)) {
            request = RequestManager.get().logCall(credentials, requestBody);
            request.enqueue(new Callback<LogCallResponse>() {
                @Override
                public void onResponse(Call<LogCallResponse> call, final Response<LogCallResponse> response) {
                    if (response.isSuccessful()) {
                        if (!isNull(response.body()) && !isNull(response.body().id)) {

                            saveCall();
                            createSavedDialog(response.body().id);

                        } else if (response.body().code == 6) {
                            showErrorDialog(context, getString(R.string.trial_expired));
                        } else
                            showErrorDialog(context, getString(R.string.unknown_error));
                    }
                }

                @Override
                public void onFailure(Call<LogCallResponse> call, Throwable t) {

                }
            });
        }
        else
            showErrorDialog(this,getString(R.string.check_internet_connection));
    }

    @Override
    public void onDismissed() {

    }

    private void createSavedDialog(final String callId){
        final Dialog dialog = new Dialog(context,R.style.Dialog);
        dialog.setContentView(R.layout.popup_logged_call);
        Window window = dialog.getWindow();
        window.setLayout(MATCH_PARENT, WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        RelativeLayout add = (RelativeLayout) dialog.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putParcelable(CONTACTS, searchResult);
                bundle.putParcelable(CALL, logCall);
                bundle.putString(CALL_ID, callId);
                Intent intent = new Intent(context, AddSummaryActivity.class);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                intent.putExtras(bundle);
                startActivity(intent);

                dialog.dismiss();
            }
        });

        RelativeLayout close = (RelativeLayout) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void saveCall(){
        ArrayList<CallArchive> list;

        if(!isNull(readCallList()))list = readCallList();
        else list = new ArrayList<>();

        CallArchive call = new CallArchive();
        call.id = logCall.id;
        call.contactId = logCall.contactId;
        call.callType = logCall.outgoing ? getString(R.string.outgoing_call) : getString(R.string.incoming_call);
        call.number = logCall.number;
        call.countryIsoCode = logCall.countryIsoCode;
        call.outgoing = logCall.outgoing;
        call.start = logCall.start;
        call.end = logCall.end;
        call.sentiment = logCall.sentiment;
        call.note = logCall.note;
        call.name = searchResult.name;
        if(!isNull(searchResult.title))
            call.company = searchResult.company;
        else
            call.company = searchResult.company;

        list.add(call);
        saveCallList(list);
    }
}

