package activities;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ondigo.R;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import dialogs.SavedDialogFragment;
import fragments.BaseDialogFragment;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import requests.RequestManager;
import requests.request_models.LogCall;
import requests.response_models.CallArchive;
import requests.response_models.LogCallResponse;
import requests.response_models.SearchResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shared_pref.Load;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static constants.Constants.Extras.CALL;
import static constants.Constants.Extras.CALL_ARCHIVE;
import static constants.Constants.Extras.CALL_ID;
import static constants.Constants.Extras.COMPANY;
import static constants.Constants.Extras.CONTACTS;
import static constants.Constants.Extras.NAME;
import static constants.Constants.Extras.NOTIFICATION;
import static constants.Constants.Extras.PHONE;
import static constants.Constants.Extras.TITLE;
import static constants.Constants.NEGATIVE;
import static constants.Constants.NEUTRAL;
import static constants.Constants.NO_DATA;
import static constants.Constants.POSITIVE;
import static constants.Constants.SAVED_POPUP_WINDOW;
import static utils.AppUtils.convertDate;
import static utils.AppUtils.dimensByRes;
import static utils.AppUtils.getVersion;
import static utils.AppUtils.isInternetConnection;
import static utils.AppUtils.isNull;
import static utils.AppUtils.showErrorDialog;
import static utils.PreferencesUtils.readCallList;
import static utils.PreferencesUtils.saveCallList;

public class AddSummaryActivity extends BaseActivity implements BaseDialogFragment.OnDialogDismissed {

    private SearchResult searchResult;
    private CallArchive callArchive;
    private String callId;
    private LogCall logCall;
    private String sentiment;
    private BaseDialogFragment savedDialog;
    private boolean outbound;
    private long time;
    private String phone;
    private LogCall tempLogCall;
    private boolean notificationFlag;
    private Load load = new Load(this);

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.positive) RelativeLayout positive;
    @BindView(R.id.positive_image) ImageView positiveImage;
    @BindView(R.id.neutral) RelativeLayout neutral;
    @BindView(R.id.neutral_image) ImageView neutralImage;
    @BindView(R.id.negative) RelativeLayout negative;
    @BindView(R.id.negative_image) ImageView negativeImage;
    @BindView(R.id.summary) EditText summary;
    @BindView(R.id.time) TextView callTime;
    @BindView(R.id.segment_view) LinearLayout segmentView;
    @BindView(R.id.outbound_segment) RelativeLayout outboundSegment;
    @BindView(R.id.inbound_segment) RelativeLayout inboundSegment;
    @BindView(R.id.outbound_segment_image) ImageView outboundSegmentImage;
    @BindView(R.id.inbound_segment_image) ImageView inboundSegmentImage;
    @BindView(R.id.outbound_segment_text) TextView outboundSegmentText;
    @BindView(R.id.inbound_segment_text) TextView inboundSegmentText;
    @BindView(R.id.save) RelativeLayout save;
    @BindView(R.id.container) RelativeLayout container;
    @BindView(R.id.scroll) ScrollView scrollView;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.save_text) TextView saveText;

    @BindColor(R.color.colorWhite) int colorWhite;
    @BindColor(R.color.colorLoginButton) int colorBlue;

    @Override
    public int getLayoutResId() {
        return R.layout.summary_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fetchExtras();

        initToolbar();
        initObjects();
        initUI();
        initListeners();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
            upArrow.setColorFilter(getResources().getColor(R.color.colorHomeButton), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            if(isNull(callArchive))
                ((TextView) toolbar.findViewById(R.id.title)).setText(searchResult.name);
            else
                ((TextView) toolbar.findViewById(R.id.title)).setText(callArchive.name);
        }
    }

    private void initUI(){
        time = System.currentTimeMillis();

        if(isNull(logCall)){
            segmentView.setVisibility(View.VISIBLE);
            callTime.setText(convertDate(System.currentTimeMillis()));
        }
        else {
            callTime.setText(convertDate(logCall.end));
            callTime.setEnabled(false);
        }

    }

    private void initObjects(){
        outbound = true;
        sentiment = NEUTRAL;
    }


    private void initListeners(){
        KeyboardVisibilityEvent.setEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if(isOpen){
                            segmentView.setVisibility(View.GONE);
                            scrollView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                                    scrollView.setVisibility(View.VISIBLE);
                                }
                            }, 10);
                        }
                        else {
                            if(isNull(logCall)) segmentView.setVisibility(View.VISIBLE);
                            scrollView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    scrollView.fullScroll(ScrollView.FOCUS_UP);
                                    scrollView.setVisibility(View.VISIBLE);
                                }
                            }, 10);
                        }
                    }
                });
    }

    private void initDateTimePickerDialog(){
        final View dialogView = View.inflate(this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        dialogView.findViewById(R.id.set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
                TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);

                Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth(),
                        timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute());

                time = calendar.getTimeInMillis();
                callTime.setText(convertDate(time));

                alertDialog.dismiss();
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.outbound_segment)
    protected void outboundCall() {
        outboundSegment.setBackgroundResource(R.drawable.segment_view_left_on);
        outboundSegmentImage.setImageResource(R.drawable.outbound_on);
        outboundSegmentText.setTextColor(colorWhite);

        inboundSegment.setBackgroundResource(R.drawable.segment_view_right_off);
        inboundSegmentImage.setImageResource(R.drawable.outbound_off);
        inboundSegmentText.setTextColor(colorBlue);

        outbound = true;
    }

    @OnClick(R.id.inbound_segment)
    protected void inboundCall() {
        outboundSegment.setBackgroundResource(R.drawable.segment_view_left_off);
        outboundSegmentImage.setImageResource(R.drawable.outbound_off);
        outboundSegmentText.setTextColor(colorBlue);

        inboundSegment.setBackgroundResource(R.drawable.segment_view_right_on);
        inboundSegmentImage.setImageResource(R.drawable.outbound_on);
        inboundSegmentText.setTextColor(colorWhite);

        outbound = false;
    }


    @OnClick(R.id.positive)
    protected void positive(){
        neutral.setBackgroundResource(R.drawable.sentiment_off);
        negative.setBackgroundResource(R.drawable.sentiment_off);
        positive.setBackgroundResource(R.drawable.sentiment_on);

        sentiment = POSITIVE;
    }

    @OnClick(R.id.neutral)
    protected void neutral(){
        neutral.setBackgroundResource(R.drawable.sentiment_on);
        negative.setBackgroundResource(R.drawable.sentiment_off);
        positive.setBackgroundResource(R.drawable.sentiment_off);

        sentiment = NEUTRAL;
    }

    @OnClick(R.id.negative)
    protected void negative(){
        neutral.setBackgroundResource(R.drawable.sentiment_off);
        negative.setBackgroundResource(R.drawable.sentiment_on);
        positive.setBackgroundResource(R.drawable.sentiment_off);

        sentiment = NEGATIVE;
    }

    @OnClick(R.id.time)
    protected void setTime(){
        initDateTimePickerDialog();
    }

    private void fetchExtras() {

        Intent intent = getIntent();
        if (!isNull(intent)) {

            callId = intent.getStringExtra(CALL_ID);
            logCall = intent.getParcelableExtra(CALL);
            phone = intent.getStringExtra(PHONE);
            callArchive = intent.getParcelableExtra(CALL_ARCHIVE);
            tempLogCall = logCall;
            notificationFlag = intent.getBooleanExtra(NOTIFICATION,false);
            searchResult = intent.getParcelableExtra(CONTACTS);

        }
    }

    @Override
    public void onBackPressed() {
        if(notificationFlag){
            Intent intent = new Intent(this, HomePageActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
            super.onBackPressed();
    }

    @OnClick(R.id.save)
    protected void save(){

        if(callTime.getText().toString().equals(getString(R.string.set_date_time))){
            Toast.makeText(this,getString(R.string.choose_date_time),Toast.LENGTH_SHORT).show();
        }
        else if(summary.getText().toString().equals(NO_DATA)){
            Toast.makeText(this,getString(R.string.enter_summary_text),Toast.LENGTH_SHORT).show();
        }
        else if(isNull(logCall)) {
            save.setEnabled(false);
            saveText.setText(NO_DATA);
            saveText.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            TelephonyManager tm = (TelephonyManager)getSystemService(getApplicationContext().TELEPHONY_SERVICE);
            String countryCode = tm.getNetworkCountryIso();

            logCall = new LogCall();
            logCall.id = NO_DATA;
            logCall.countryIsoCode = countryCode;
            logCall.outgoing = outbound;
            logCall.start = time;
            logCall.end = time;
            logCall.note = summary.getText().toString();
            logCall.sentiment = sentiment;

            if(isNull(callArchive)) {
                logCall.contactId = searchResult.id;
                logCall.number = phone;
            }
            else{
                logCall.contactId = callArchive.contactId;
                logCall.number = callArchive.number;
            }
            logCall.version = getVersion(this);

            createRequestBody(logCall);

        }
        else{
            save.setEnabled(false);
            saveText.setText(NO_DATA);
            saveText.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

            logCall.id = callId;
            logCall.note = summary.getText().toString();
            logCall.sentiment = sentiment;
            logCall.version = getVersion(this);
            createRequestBody(logCall);
        }
    }

    private void createRequestBody(LogCall object){
        String content = new Gson().toJsonTree(object).toString();
        Log.wtf("json",content);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), content);
        logCall(load.loadSession(),requestBody);
    }

    private void logCall(String credentials, final RequestBody requestBody){
        if(isInternetConnection(this)) {
            RequestManager.get().logCall(credentials, requestBody).enqueue(new Callback<LogCallResponse>() {

                @Override
                public void onResponse(Call<LogCallResponse> call, final Response<LogCallResponse> response) {

                    Log.wtf("call",call.request().url().toString());
                    if (response.isSuccessful()) {
                        logCall.id = response.body().id;

                        if (!isNull(response.body()) && !isNull(response.body().id)) {
                            if (!isNull(callArchive) || isNull(tempLogCall)) saveCall();
                            savedDialog = new SavedDialogFragment();
                            savedDialog.setOnDismissedListener(AddSummaryActivity.this);
                            savedDialog.show(getSupportFragmentManager(), SAVED_POPUP_WINDOW);

                        } else if (response.body().code == 6) {
                            showErrorDialog(AddSummaryActivity.this, getString(R.string.trial_expired));
                        } else
                            showErrorDialog(AddSummaryActivity.this, getString(R.string.unknown_error));

                        saveText.setText(R.string.save);
                        saveText.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        save.setEnabled(true);
                    } else {
                        saveText.setText(R.string.save);
                        saveText.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        save.setEnabled(true);
                    }
                }

                @Override
                public void onFailure(Call<LogCallResponse> call, Throwable t) {
                    saveText.setText(R.string.save);
                    saveText.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    save.setEnabled(true);
                }
            });
        }
        else{
            showErrorDialog(this,getString(R.string.check_internet_connection));
            saveText.setText(R.string.save);
            saveText.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            save.setEnabled(true);
        }
    }

    @Override
    public void onDismissed() {}

    private void saveCall(){
        ArrayList<CallArchive> list;

        if(!isNull(readCallList()))list = readCallList();
        else list = new ArrayList<>();

        CallArchive call = new CallArchive();
        call.id = logCall.id;
        call.contactId = logCall.contactId;
        call.callType = outbound ? getString(R.string.outgoing_call) : getString(R.string.incoming_call);
        call.number = logCall.number;
        call.countryIsoCode = logCall.countryIsoCode;
        call.outgoing = logCall.outgoing;
        call.start = logCall.start;
        call.end = logCall.end;
        call.sentiment = logCall.sentiment;
        call.note = logCall.note;
        call.name = searchResult.name;
        call.company = searchResult.company;

        list.add(call);
        saveCallList(list);
    }

}
