package activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ondigo.R;

import java.util.ArrayList;

import adapters.ContactListAdapter;
import butterknife.BindView;
import fragments.BaseDialogFragment;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import requests.RequestManager;
import requests.request_models.LogCall;
import requests.response_models.CallArchive;
import requests.response_models.LogCallResponse;
import requests.response_models.Phone;
import requests.response_models.SearchResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shared_pref.Load;


import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static constants.Constants.Extras.CALL;
import static constants.Constants.Extras.CALL_ID;
import static constants.Constants.Extras.CONTACTS;
import static constants.Constants.Extras.END;
import static constants.Constants.Extras.OUTBOUND;
import static constants.Constants.Extras.PHONE;
import static constants.Constants.Extras.START;
import static constants.Constants.NEUTRAL;
import static constants.Constants.NO_DATA;
import static utils.AppUtils.getVersion;
import static utils.AppUtils.isInternetConnection;
import static utils.AppUtils.isNull;
import static utils.AppUtils.showErrorDialog;
import static utils.PreferencesUtils.readCallList;
import static utils.PreferencesUtils.saveCallList;

public class ContactsActivity extends BaseActivity implements BaseDialogFragment.OnDialogDismissed {

    private SearchResult searchResult;
    private ContactListAdapter listAdapter;
    private LogCall logCall;
    private Call<LogCallResponse> request;
    private Context context;
    private Load load = new Load(this);

    @BindView(R.id.list) ListView list;
    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    public int getLayoutResId() {
        return R.layout.contacts_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(OUTBOUND);

        registerReceiver(callDataReceiver, intentFilter);

        fetchExtras();
        initToolbar();
        initObjects();
    }


    private final BroadcastReceiver callDataReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(OUTBOUND)) {
                String number = intent.getStringExtra(PHONE);
                long start = intent.getLongExtra(START, 0);
                long end = intent.getLongExtra(END, 0);

                TelephonyManager tm = (TelephonyManager)getSystemService(getApplicationContext().TELEPHONY_SERVICE);
                String countryCode = tm.getNetworkCountryIso();
                logCall = new LogCall();
                logCall.id = NO_DATA;
                logCall.contactId = searchResult.id;
                logCall.countryIsoCode = countryCode;
                logCall.outgoing = true;
                logCall.missed = false;
                logCall.start = start;
                logCall.end = end;
                logCall.sentiment = NEUTRAL;
                logCall.note = NO_DATA;
                logCall.number = number;
                logCall.version = getVersion(context);

                String content = new Gson().toJsonTree(logCall).toString();
                Log.wtf("json",content);
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), content);
                logCall(load.loadSession(),requestBody);
            }
        }

    };

    @Override
    protected void onDestroy(){
        super.onDestroy();
        try  {
            unregisterReceiver(callDataReceiver);
        }
        catch (IllegalArgumentException e) {

        }

        if(!isNull(request))
            request.cancel();
    }

    private void fetchExtras() {
        Intent intent = getIntent();
        boolean flagEmail = true;

        if (!isNull(intent)) {
            searchResult = intent.getParcelableExtra(CONTACTS);
            Phone phone = new Phone();
            phone.label = getString(R.string.email);

            flagEmail = !isNull(searchResult.email);

            if(flagEmail) {
                phone.value = searchResult.email;
                searchResult.phones.add(phone);
            }

            phone = new Phone();
            phone.label = getString(R.string.salesforce_contact);
            phone.value = getString(R.string.view_in_salesforce);
            searchResult.phones.add(phone);

            listAdapter = new ContactListAdapter(this,searchResult.phones,searchResult.url,flagEmail);
            list.setAdapter(listAdapter);
        }
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back);
            upArrow.setColorFilter(getResources().getColor(R.color.colorHomeButton), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            ((TextView) toolbar.findViewById(R.id.title)).setText(searchResult.name);
        }
    }

    private void initObjects(){
        context = this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logCall(String credentials, final RequestBody requestBody){
        if(isInternetConnection(this)) {
            request = RequestManager.get().logCall(credentials, requestBody);
            request.enqueue(new Callback<LogCallResponse>() {

                @Override
                public void onResponse(Call<LogCallResponse> call, final Response<LogCallResponse> response) {
                    if (response.isSuccessful()) {
                        if (!isNull(response.body()) && !isNull(response.body().id)) {

                            saveCall();
                            createSavedDialog(response.body().id);

                        } else if (response.body().code == 6) {
                            showErrorDialog(context, getString(R.string.trial_expired));
                        } else
                            showErrorDialog(context, getString(R.string.unknown_error));
                    }
                }

                @Override
                public void onFailure(Call<LogCallResponse> call, Throwable t) {
                }
            });
        }
        else{
            showErrorDialog(this,getString(R.string.check_internet_connection));
        }
    }

    @Override
    public void onDismissed() {

    }

    private void createSavedDialog(final String callId){
        final Dialog dialog = new Dialog(context,R.style.Dialog);
        dialog.setContentView(R.layout.popup_logged_call);
        Window window = dialog.getWindow();
        window.setLayout(MATCH_PARENT, WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        RelativeLayout add = (RelativeLayout) dialog.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putParcelable(CONTACTS, searchResult);
                bundle.putParcelable(CALL, logCall);
                bundle.putString(CALL_ID, callId);
                Intent intent = new Intent(context, AddSummaryActivity.class);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                intent.putExtras(bundle);
                startActivity(intent);

                dialog.dismiss();
            }
        });

        RelativeLayout close = (RelativeLayout) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void saveCall(){
        ArrayList<CallArchive> list;

        if(!isNull(readCallList()))list = readCallList();
        else list = new ArrayList<>();

        CallArchive call = new CallArchive();
        call.id = logCall.id;
        call.contactId = logCall.contactId;
        call.callType = logCall.outgoing ? getString(R.string.outgoing_call) : getString(R.string.incoming_call);
        call.number = logCall.number;
        call.countryIsoCode = logCall.countryIsoCode;
        call.outgoing = logCall.outgoing;
        call.start = logCall.start;
        call.end = logCall.end;
        call.sentiment = logCall.sentiment;
        call.note = logCall.note;
        call.name = searchResult.name;
        call.company = searchResult.company;

        list.add(call);
        saveCallList(list);
    }
}
