package activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.ondigo.R;

import butterknife.OnClick;
import shared_pref.Load;

import static constants.Constants.NO_DATA;

public class LoginActivity extends BaseActivity{

    private Load load = new Load(this);

    @Override
    public int getLayoutResId() {
        return R.layout.login_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestPermissions();

        if(!load.loadSession().equals(NO_DATA)) {
            startActivity(new Intent(this,HomePageActivity.class));
            finish();
        }

    }

    @OnClick(R.id.sign_in)
    protected void signIn(){
        startActivity(new Intent(this,SalesforceActivity.class));
    }

    public void requestPermissions(){
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.CLEAR_APP_CACHE,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.READ_CALL_LOG};

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }
    public boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
