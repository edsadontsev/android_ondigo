package activities;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import interfaces.IViewsBinder;

import static utils.AppUtils.isNull;

public abstract class BaseActivity extends AppCompatActivity implements IViewsBinder {
    private Unbinder mUnbinder;

    public abstract int getLayoutResId();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());

        bindViews();
    }

    @Override
    protected void onDestroy() {
        unbindViews();
        super.onDestroy();
    }

    @Override
    public void bindViews() {
        mUnbinder = ButterKnife.bind(this);
    }

    @Override
    public void unbindViews() {
        if (!isNull(mUnbinder)) mUnbinder.unbind();
    }
}

