package activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ondigo.R;

import butterknife.BindView;
import butterknife.OnClick;
import dialogs.LogOutDialogFragment;
import dialogs.LoggedCallDialogFragment;
import dialogs.SavedDialogFragment;
import fragments.BaseDialogFragment;
import requests.RequestManager;
import requests.request_models.LogCall;
import requests.response_models.LogCallResponse;
import requests.response_models.SearchResult;
import requests.response_models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import shared_pref.Load;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static constants.Constants.Extras.CALL;
import static constants.Constants.Extras.CALL_FLAG;
import static constants.Constants.Extras.CALL_ID;
import static constants.Constants.Extras.CONTACTS;
import static constants.Constants.Extras.TRIAL;
import static constants.Constants.LOGGED_CALL_POPUP_WINDOW;
import static constants.Constants.LOGOUT_POPUP_WINDOW;
import static constants.Constants.SAVED_POPUP_WINDOW;
import static utils.AppUtils.getVersion;
import static utils.AppUtils.isInternetConnection;
import static utils.AppUtils.isNull;
import static utils.AppUtils.showErrorDialog;

public class HomePageActivity extends BaseActivity implements BaseDialogFragment.OnDialogDismissed {

    private Context context;
    private BaseDialogFragment logoutDialog;
    private BaseDialogFragment loggedCallDialog;
    private Load load = new Load(this);

    @BindView(R.id.photo) ImageView photo;
    @BindView(R.id.name) TextView name;
    @BindView(R.id.company) TextView company;
    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    public int getLayoutResId() {
        return R.layout.home_page_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.wtf("version",getVersion(this));

        requestPermissions();
        fetchExtras();

        initObjects();
        initUI();
        getUser(load.loadSession());
    }

    private void initObjects(){
        context = this;
    }

    private void initUI(){
        RelativeLayout menu = (RelativeLayout) toolbar.findViewById(R.id.menu);
        menu.setVisibility(View.VISIBLE);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutDialog = new LogOutDialogFragment();
                logoutDialog.setOnDismissedListener(HomePageActivity.this);
                logoutDialog.show(getSupportFragmentManager(), LOGOUT_POPUP_WINDOW);
            }
        });
    }

    private void getUser(String credentials){
        if(isInternetConnection(this)) {
            RequestManager.get().getUser(credentials).enqueue(new Callback<User>() {

                @Override
                public void onResponse(Call<User> call, final Response<User> response) {

                    if (response.isSuccessful()) {
                        Glide.with(getApplicationContext())
                                .load(response.body().avatar)
                                .crossFade()
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(photo);

                        name.setText(response.body().name);
                        company.setText(response.body().company);
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {

                }
            });
        }
        else
            showErrorDialog(this,getString(R.string.check_internet_connection));
    }

    @OnClick({R.id.make_call_top,R.id.make_call_bottom})
    protected void makeCallSearch(){
        Bundle bundle = new Bundle();
        bundle.putBoolean(CALL_FLAG, true);
        Intent intent = new Intent(this, SearchActivity.class);
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @OnClick({R.id.log_call_top,R.id.log_call_bottom})
    protected void logCallSearch(){
        Bundle bundle = new Bundle();
        bundle.putBoolean(CALL_FLAG, false);
        Intent intent = new Intent(this, SearchActivity.class);
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onDismissed() {

    }

    private void fetchExtras() {
        Intent intent = getIntent();
        if (!isNull(intent)) {

            if(!isNull(intent.getParcelableExtra(CALL))) {

                Log.wtf("bbbbb","bbbbb");
                LogCall logCall = intent.getParcelableExtra(CALL);
                String callId = intent.getStringExtra(CALL_ID);
                SearchResult searchResult = intent.getParcelableExtra(CONTACTS);

                Bundle bundle = new Bundle();
                bundle.putParcelable(CONTACTS, searchResult);
                bundle.putString(CALL_ID, callId);
                bundle.putParcelable(CALL, logCall);

                loggedCallDialog = new LoggedCallDialogFragment();
                loggedCallDialog.setArguments(bundle);
                loggedCallDialog.setOnDismissedListener(HomePageActivity.this);
                loggedCallDialog.show(getSupportFragmentManager(), LOGGED_CALL_POPUP_WINDOW);
            }
            else if (!isNull(intent.getParcelableExtra(TRIAL))){

                Log.wtf("aaaaa","aaaaa");

                Bundle bundle = new Bundle();
                bundle.putBoolean(TRIAL, true);

                loggedCallDialog = new SavedDialogFragment();
                loggedCallDialog.setArguments(bundle);
                loggedCallDialog.setOnDismissedListener(HomePageActivity.this);
                loggedCallDialog.show(getSupportFragmentManager(), SAVED_POPUP_WINDOW);
            }
        }
    }

    public void requestPermissions(){
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.CLEAR_APP_CACHE,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.READ_CALL_LOG};

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }
    public boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
